# Rezo 4o4
> Private Social Network like twitter

Rezo 4o4 is a private social network like twitter, companies can use it to communicate with their teams.

It is a private space, it is necessary to be connected to use it. Once logged in, you will be able to follow the people you want to see in order to see their publications on the home page.
You can interact with users' posts either by replying or with likes.

![](https://4o4.fr/images/rezo.png)

## Installation

Before:

* create .env file in backend folder
* Add these variables:
    * PORT = <YOUR BACKEND PORT>
    * DB_URL = <YOUR MONGODB URL>
    * JWT = <YOUR SECRET KEY>

Frontend:

```sh
cd frontend && npm install && npm start
```

Backend:

```sh
cd backend && npm install && nodemon
```

## API Documentation

Soon

## Components Stories

Soon

## Meta

Mathieu Piton – contact@mathieu-piton.com

Distributed under the MIT license. See ``LICENSE`` for more information.

[https://gitlab.com/mpiton](https://gitlab.com/mpiton)
