const express = require("express")
const mongoose = require("mongoose")
const cors = require("cors")
const bodyParser = require("body-parser")
const cookieParser = require("cookie-parser")
const morgan = require("morgan")

//import routes
const userRoutes = require("./routes/user")
const postRoutes = require("./routes/post")

const app = express()
app.use(morgan('tiny'));

require('dotenv').config()

//middleware
app.use(cors())
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({
    limit: "50mb",
    extended: true,
    parameterLimit: 50000
}))
app.use(cookieParser())

//env
const port = process.env.PORT
const database = process.env.DB_URL

//connect to database
const connection = async ()=> {
    try {
        await mongoose.connect(database, {
            useNewUrlParser: true,
            useUnifiedTopology: true,
            useCreateIndex: true,
            useFindAndModify: false
        })
        console.log("connected to the database");
    } catch (error) {
        if (error) throw error
    }
}
connection()

//Use routes
app.use("/", userRoutes)
app.use("/", postRoutes)

//homepage
app.get("/", function(req, res) {
    res.send("it's work")
})

//start server
app.listen(port, (err)=> {
    if (err) console.log(err)
    console.log(`server started at port ${port}`);
})