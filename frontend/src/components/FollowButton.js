import { faHeart, faHeartBroken } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import React from 'react'
import { Link } from 'react-router-dom'
import { subscribe, unsubscribe } from '../redux/actions/userActions'

export default function FollowButton({handleButtonClick, following, userId, token, followId}) {

    async function followUser(){
        const userData = await subscribe(userId,followId, token)
            if (userData.error){
                console.log(userData.error)
            } else {
                handleButtonClick(userData.data)
            }
    }

    async function unfollowUser(){
        const userData = await unsubscribe(userId,followId, token)
            if (userData.error){
                console.log(userData.error)
            } else {
                handleButtonClick(userData.data)
            }
    }

    return (
        <div>
            {following ?
                <Link onClick={()=> unfollowUser()}><span class="tag is-primary is-light has-text-weight-bold">Unsubscribe &nbsp; <FontAwesomeIcon style={{color: "crimson"}} icon={faHeartBroken}></FontAwesomeIcon></span></Link>
                :
                <Link onClick={()=> followUser()}><span  class="tag is-primary is-light has-text-weight-bold">Subscribe &nbsp; <FontAwesomeIcon style={{color: "crimson"}} icon={faHeart}></FontAwesomeIcon></span></Link>
            }
        </div>
    )
}
