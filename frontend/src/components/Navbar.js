
import {Link, useHistory} from "react-router-dom"
import {logout } from '../helpers/auth'
import logo from "../logo.png"

export default function Navbar({currentUser}) {

    const history = useHistory()


    return (
        <nav className="navbar" role="navigation" aria-label="main navigation">
        <div className="navbar-brand">
            <img src={logo} alt='logo' width="213" height="60"/>
          <Link to="#" role="button" className="navbar-burger" aria-label="menu" aria-expanded="false" data-target="navbarBasicExample">
            <span aria-hidden="true"></span>
            <span aria-hidden="true"></span>
            <span aria-hidden="true"></span>
          </Link>
        </div>
      
        <div id="navbarBasicExample" className="navbar-menu">
          <div className="navbar-start">
            <Link to="/" className="navbar-item">
              Home
            </Link>
      
            <Link to="#" className="navbar-item">
              Documentation
            </Link>

            <Link to="/users" className="navbar-item">
              Users
            </Link>
      
            <div className="navbar-item has-dropdown is-hoverable">
              <Link to="#" className="navbar-link">
                My account
              </Link>
              <div className="navbar-dropdown">
                {
                  !currentUser ? (
                  <>
                  <Link to="/register" className="navbar-item">
                    Register
                  </Link>
                  <Link to="login" className="navbar-item">
                    Sign In
                  </Link>
                  </>
                  ) : (
                  <>
                  <Link to={`/user/${currentUser && currentUser.user && currentUser.user._id}`} className="navbar-item">
                    @{currentUser && currentUser.user.name}
                  </Link>
                  <Link
                  to="#"
                  onClick={()=> {
                    logout(()=> {
                      history.push("/login")
                    })
                  }}
                  className="navbar-item">
                    Sign Out
                  </Link>
                  </>
                )}
              </div>
            </div>
          </div>

          <div className="navbar-end">
            <div className="navbar-item">
              {
                !currentUser ?
                <div className="buttons">
                <Link to="/register" className="button is-primary">
                  <strong>Sign up</strong>
                </Link>
                <Link to="/login" className="button is-light">
                  Log in
                </Link>
              </div>
              :
              <div className="buttons">
              <Link to={`/user/${currentUser && currentUser.user && currentUser.user._id}`} className="button is-primary">
                <strong>My Profile</strong>
              </Link>
              <Link
              to="#"
              className="button is-light"
              onClick={()=> {
                logout(()=> {
                  history.push("/login")
                })
              }}
              >
                Logout
              </Link>
            </div>
              }
            </div>
          </div>
        </div>
      </nav>
    )
}
