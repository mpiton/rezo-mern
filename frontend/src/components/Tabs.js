import { faCommentAlt, faThumbsUp, faUsers } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import React, { Fragment, useState } from 'react'
import { Link } from 'react-router-dom';
import FeedComponent from './FeedComponent';
import FollowerComponent from './FollowerComponent';
import FollowingComponent from './FollowingComponent';

export default function Tabs({followers, following}) {
 
  const [currentTab, setCurrentTab] = useState('feed');
  // eslint-disable-next-line no-unused-vars
  const [currentIcon, setCurrentIcon] = useState('faCommentAlt');

  function mySetter(tab) {
    setCurrentTab(tab.name)
    setCurrentIcon(tab.icon)
  }
  const tabList = [
    {
      name: 'feed',
      label: 'My publications',
      icon: faCommentAlt,
      content: (<FeedComponent/>),
 
    },
    {
      name: 'followers',
      label: 'Followers',
      icon: faUsers,
      content: (<FollowerComponent followers={followers}/>),
    
    },
    {
      name: 'following',
      label: 'Following',
      icon: faThumbsUp,
      content: (<FollowingComponent following={following}/>),
    }
  ]
  
  return (
    <Fragment>
    <div className="tabs is-centered is-boxed">
  <ul>
        {
          tabList.map((tab, i) => (
            <li
              key={i}
              onClick={() => mySetter(tab)}
              className={(tab.name === currentTab) ? 'is-active' : ''}>
                <Link>
        <span className="icon is-small"><FontAwesomeIcon icon={tab.icon}/></span>
        <span>{tab.label}</span>
      </Link>
            </li>
          ))
        }
        </ul>
        </div>
        <div className="card" style={{maxWidth: "70%", margin: "0 auto"}}>
      {
        tabList.map((tab, i) => {
          if(tab.name === currentTab) {
            return <div className="card-content" key={i}>{tab.content}</div>;
          } else {
            return null;
          }
        })
      }
    
      
    </div>
    </Fragment>
  )
}