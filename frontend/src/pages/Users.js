import React, { useEffect, useState } from 'react';
import { connect, useDispatch } from 'react-redux';
import { Link } from 'react-router-dom';
import { isLogged } from '../helpers/auth';
import { getAllUsers } from '../redux/actions/userActions';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faAngleDoubleRight } from '@fortawesome/free-solid-svg-icons'

function Users({ users, userError }) {
	const dispatch = useDispatch();

	const [ error, setError ] = useState(null);

	const jwt = isLogged();

	useEffect(
		() => {
			if (userError && userError !== null) setError(userError);
			dispatch(getAllUsers(jwt && jwt.token));
		},
		[dispatch, jwt, userError]
	);

	const showError = () => {
		return (
			error && (
				<div className="notification is-danger">
					<button class="delete" />
					{error}
				</div>
			)
		);
	};

	if (!jwt) {
        return <div className="container">
        <div class="card">
  <div class="card-content">
    <p style={{textAlign: "center"}} class="subtitle">
    Please <Link to="/login">log in</Link> to view the users
    </p>
  </div>
</div>
</div>
    }

	return (
		<div className="card" style={{maxWidth: "70%", margin: "0 auto"}}>
			<header className="card-header">
				{showError()}
				<p className="card-header-title notification is-primary">
					<h3 className="title is-3">Profiles</h3>
				</p>
			</header>
			<div className="card-content">
				{users &&
					users.map((user, index) => (
                        <Link to={`/user/${user._id}`}
                        key={index}
                        style={{textDecoration: "none"}}>
						<div className="media mb-1">
							<div class="media-left">
								<figure className="image is-48x48">
									<img
										src={`http://localhost:8888/api/user/photo/${user._id}`}
										alt={`profile of ${user && user.name}`}
									/>
								</figure>
							</div>
							<div className="media-content">
								<p className="title is-4" key={index}>
									{user.name}
								</p>
								<p className="subtitle is-6">@{user.name}</p>
							</div>
                            <Link to="#" className="card-header-icon" aria-label="more options">
      <span className="icon">
      <FontAwesomeIcon className="has-text-primary" aria-hidden="true" icon={faAngleDoubleRight} />
      </span>
    </Link>
						</div>
                        <hr/>
                        </Link>
                        
					))}
			</div>
		</div>
	);
}

const mapStateToProps = ({ user: { users, userError } }) => ({
	users,
	userError
});

export default connect(mapStateToProps, null)(Users);
