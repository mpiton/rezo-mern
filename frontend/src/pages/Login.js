import React, { useEffect, useState } from 'react'
import { Redirect } from 'react-router-dom'
import {useDispatch, connect} from "react-redux"
import {login } from "../redux/actions/userActions"

function Register({userError, userSuccess}) {
    const [user, setUser] = useState({
        email: "",
        password: ""
    })

    const [error, setError] = useState(null);
    const [success, setSuccess] = useState(false)

    const dispatch = useDispatch()

    useEffect(() => {
      if (userError && userError !== null) setError(userError)
      if (userSuccess) {
        setSuccess(userSuccess)
        dispatch({type: "TOGGLE_SUCCESS"})
      }
      return ()=> {
        dispatch({type: "HIDE_USER_ERROR"})
      }
    }, [dispatch, userError, userSuccess])

    const handleInputChange = (event)=> {
      setError("")
        setUser({...user,[event.target.name]: event.target.value})
    }

    const showError = ()=> {
      return error && (
        <div className="notification is-danger">
          <button class="delete"></button>
          {error}
        </div>
      )
    }

    const redirectUser = () => {
      return success && <Redirect to="/"/>
    }

    const handleFormSubmit = (event) => {
        event.preventDefault()
        //console.log(user);
        dispatch(login(user))
    }

    return (
        <React.Fragment>
            <article className="message is-primary">
            <div className="message-header">
              <p>Login</p>
            </div>
        <div className="message-body">
        <form onSubmit={handleFormSubmit}>
        <div className="columns is-tablet">
      <div className="column is-one-quarter "></div>
      <div className="column custom-color">
      {showError()}
        {redirectUser()}
        <div className="field">
          <label className="label">Email</label>
          <div className="control ">
            <input
                required
                value={user.email}
                onChange={(event)=> handleInputChange(event)}
                name="email"
                className="input is-primary"
                type="email"
                placeholder="Email"
            />
            <span className="icon is-small is-left">
            <i className="fas fa-envelope"></i>
            </span>
            <span className="icon is-small is-right">
            <i className="fas fa-exclamation-triangle"></i>
            </span>
          </div>
        </div>
        <div className="field">
          <label className="label">Password</label>
          <div className="control ">
            <input
                required
                name="password"
                value={user.password}
                onChange={(event)=>handleInputChange(event)}
                className="input is-primary"
                type="password"
                placeholder="Password"
            />
            <span className="icon is-small is-left">
            <i className="fas fa-envelope"></i>
            </span>
            <span className="icon is-small is-right">
            <i className="fas fa-exclamation-triangle"></i>
            </span>
          </div>
        </div>
        <div className="field is-grouped pt-5">
          <div className="control">
            <button className="button submit">Login</button>
          </div>
        </div>
      </div>
      <div className="column is-one-quarter"></div>
    </div>
    </form>
  </div>
</article>
    </React.Fragment>
    )
}

const mapStateToProps = ({
  user: {
    userError,
    userSuccess
  }
}) => ({
  userError,
  userSuccess
})

export default connect(mapStateToProps,null)(Register)