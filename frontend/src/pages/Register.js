import React, { useEffect, useState } from 'react'
import { Link, Redirect } from 'react-router-dom'
import {useDispatch, connect} from "react-redux"
import {createUser } from "../redux/actions/userActions"
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCheck, faUser } from '@fortawesome/free-solid-svg-icons'

function Register({userError, userSuccess}) {
    const [user, setUser] = useState({
        name: "",
        email: "",
        password: ""
    })

    const [error, setError] = useState(null);
    const [success, setSuccess] = useState(false)
    const [validForm, setValidForm] = useState(true)
    const [iAgree, setIAgree] = useState(false)

    const dispatch = useDispatch()

    useEffect(() => {
      if (userError && userError !== null) setError(userError)
      if (userSuccess) {
        setSuccess(userSuccess)
        dispatch({type: "TOGGLE_SUCCESS"})
      }
      return ()=> {
        dispatch({type: "HIDE_USER_ERROR"})
      }
    }, [dispatch, user, userError, userSuccess])

    //listener
    const handleInputChange = event => {
      setError("")
      console.log(validForm);
      event.target.checked ? setIAgree(true) : setIAgree(false)
      const { name, value } = event.target;
      setUser({ ...user, [name]: value });
   };

    const showError = ()=> {
      return error && (
        <div class="notification is-danger">
          {error}
        </div>
      )
    }

    //if user create -> redirect /login
    const redirectUser = () => {
      return success && <Redirect to="/login"/>
    }

    //submit the form
    const handleFormSubmit = (event) => {
        event.preventDefault()
        validateForm(user)
    }

    //validation
    const validateForm = (user) => {
      var data = {
        name: user.name,
        email: user.email,
        password: user.password,
        checkbox: iAgree
    };

        //Test Name
        if(data.name !== "") {
        let pattern = /^[a-zA-Zçéèëêîïä\- ]*$/;
        let result = pattern.test(data.name);
        if (!result) {
          setError("Please enter alphabet characters only.")
          return setValidForm(false)
        }
        }

        //Test Email
        if (data.email !== "") {
          //regular expression for email validation
          let pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
          if (!pattern.test(data.email)) {
            setError("Please enter valid email.")
            return setValidForm(false)
          }
        }

        //Test Password
        if (data.password !== "") {
          //regular expression for password
          let pattern = new RegExp(/^((?=\S*?[A-Z])(?=\S*?[a-z])(?=\S*?[0-9]).{7,})\S$/);
          if (!pattern.test(data.password)) {
            setError("Please enter secure and strong password. (min 8 characters with uppercase letters and numbers)")
            return setValidForm(false)
          }
        }

        //Accept terms and conditions
        if (data.checkbox === true) {
          setError("You must accept the terms and conditions")
          return setValidForm(false)
        }

        //if all data are correct -> create user
        return dispatch(createUser(user))
        }


    return (
        <React.Fragment>
            <article className="message is-primary">
            <div className="message-header">
              <p>Register</p>
            </div>
        <div className="message-body">
        <form onSubmit={handleFormSubmit}>
        <div className="columns is-tablet">
      <div className="column is-one-quarter "></div>
      <div className="column custom-color">
      {showError()}
        {redirectUser()}
        <div className="field">
          <label className="label">Name</label>
          <div className="control has-icons-left has-icons-right">
            <input
                name="name"
                value={user.name}
                onChange={(event)=> handleInputChange(event)}
                className="input is-primary"
                type="text"
                placeholder="Name"
            />
            <span className="icon is-small is-left">
            <FontAwesomeIcon icon={faUser} />
            </span>
            <span className="icon is-small is-right">
            <FontAwesomeIcon icon={faCheck}/>
            </span>
          </div>
        </div>
        <div className="field">
          <label className="label">Email</label>
          <div className="control ">
            <input
                required
                value={user.email}
                name="email"
                onChange={(event)=> handleInputChange(event)}
                className="input is-primary"
                type="email"
                placeholder="Email"
            />
            <span className="icon is-small is-left">
            <i className="fas fa-envelope"></i>
            </span>
            <span className="icon is-small is-right">
            <i className="fas fa-exclamation-triangle"></i>
            </span>
          </div>
        </div>
        <div className="field">
          <label className="label">Password</label>
          <div className="control ">
            <input
                required
                name="password"
                value={user.password}
                onChange={(event)=>handleInputChange(event)}
                className="input is-primary"
                type="password"
                placeholder="Password"
            />
            <span className="icon is-small is-left">
            <i className="fas fa-envelope"></i>
            </span>
            <span className="icon is-small is-right">
            <i className="fas fa-exclamation-triangle"></i>
            </span>
          </div>
        </div>
        <div className="field pt-3">
          <div className="control">
            <label className="checkbox">
            <input
            type="checkbox"
            required
            name="checkbox"
            onChange={(event)=>handleInputChange(event)}
            defaultChecked={iAgree}
            value={user.checkbox}
            />
            I agree to the <Link to="/">terms and conditions</Link>
            </label>
          </div>
        </div>
          <div className="field is-grouped pt-5">
          <div className="control">
            <button className="button submit">Register</button>
          </div>
        </div>
      </div>
      <div className="column is-one-quarter"></div>
    </div>
    </form>
  </div>
</article>
    </React.Fragment>
    )
}

const mapStateToProps = ({
  user: {
    userError,
    userSuccess
  }
}) => ({
  userError,
  userSuccess
})

export default connect(mapStateToProps,null)(Register)