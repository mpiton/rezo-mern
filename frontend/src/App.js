import Home from './pages/Home';
import {BrowserRouter, Route,Switch} from "react-router-dom"
import Navbar from './components/Navbar';
import Register from './pages/Register';
import Login from './pages/Login';
import { connect, useDispatch } from 'react-redux';
import { useEffect } from 'react';
import { authCheck } from './redux/actions/userActions';
import Users from './pages/Users';
import Profile from './pages/Profile';
import EditProfile from './pages/EditProfile';


function App({currentUser}) {
  const dispatch = useDispatch()

  useEffect(() => {
    dispatch(authCheck())
  }, [dispatch])

  return (
    <BrowserRouter>
      <Navbar currentUser={currentUser && currentUser}/>
      <Switch>
        <Route path="/" exact component={Home}/>
        <Route path="/register" exact component={Register}/>
        <Route path="/login" exact component={Login}/>
        <Route path="/users" exact component={Users}/>
        <Route path="/user/:userId" exact component={Profile}/>
        <Route path="/user/edit/:userId" exact component={EditProfile}/>
      </Switch>
    </BrowserRouter>
  );
}

const mapStateToProps = ({user: {currentUser}}) => ({
  currentUser
})

export default connect(mapStateToProps)(App);
